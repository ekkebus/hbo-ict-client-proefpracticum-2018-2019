# HBO-ICT Proefpracticum 2018-2019

Run instructions

Install git first: https://git-scm.com/downloads

```
$ git clone https://gitlab.com/ekkebus/hbo-ict-client-proefpracticum-2018-2019
```

Installed NPM packages
Je hebt een installatie van node.js nodig om NPM te kunnen runnen (https://nodejs.org/en/)
```
npm install --save-dev gulp-cli gulp gulp-concat browser-sync gulp-autoprefixer gulp-clean-css
```

# Tip van Flip
Zorg bij het maken van de toets dat je altijd papier en een pen bij de hand hebt, je mag gewoon aantekeningen maken op je opgavenblad.
