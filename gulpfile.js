const gulp = require('gulp');
const {src, dest} = require('gulp');
const {series, parallel} = require('gulp');
const clean_css = require('gulp-clean-css');
const auto_prefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();

/**
 * Opdracht 1a tot en met 1d
 * @returns {*}
 */
const js = function () {
    //...
}

const css = function () {
    //...
}

const html = function () {
    //...
}

//exports.build = ...
